**About**

This program uses a [Winsen ZH03](https://www.winsen-sensor.com/sensors/dust-sensor/zh3b.html) particle
sensor as a [Homie Node](https://homieiot.github.io/) on an [ESP8266 WiFi](https://www.sparkfun.com/products/17146) board.
It runs continuously, reporting the counts over [MQTT](https://mqtt.org/) at a configurable interval.

It works best with [Homie for ESP8266 / ESP32 v3](https://github.com/homieiot/homie-esp8266/tree/v3.0)

---

## Basic Operation

The Winsen ZH03 sensor outputs a 3.3V-serial payload that contains information about
density of 1, 2.5 and 10 μm particles in the air. The ZH03 has a built-in fan that 
ensures a constant airflow and is factory calibrated to μm/m<sup>3</sup>.

The interval (in seconds) is set via the "interval" setting in the config.json

```json
{
    "name": "Dust Particle Sensor",
    "device_id": "dust-2",
    "device_stats_interval": 300,
    "wifi": {
      "ssid": "<SSID>",
      "password": "<Password>"
    },
    "mqtt": {
      "host": "<MQTT broker IP>",
      "port": 1883,
      "base_topic": "<base topic>/"
    },
    "ota": {
      "enabled": true
    },
    "settings": {
      "interval" : 60
    }
}
```

The config.json must be installed to the ESP8266, e.g. using
[ESP8266FS-0.5.0](https://github.com/esp8266/arduino-esp8266fs-plugin/releases/download/0.5.0/ESP8266FS-0.5.0.zip)
to upload the configuration to the ESP to /homie/config.json, 
[as documented here](https://arduino-esp8266.readthedocs.io/en/2.7.4_a/filesystem.html#uploading-files-to-file-system)

See [JSON configuration file](https://homieiot.github.io/homie-esp8266/docs/3.0.1/configuration/json-configuration-file/).

## Compilation

This project is intended for the Arduino IDE.

Clone this repository and load the "Homiev3_DustSensor_ZH03" project into the Arduino IDE.

You will need to manually install the Homie [dependencies](https://homieiot.github.io/homie-esp8266/docs/3.0.1/quickstart/getting-started/).

Examples that worked for me are:

1. [ArduinoJson >= 5.0.8](https://github.com/bblanchon/ArduinoJson/archive/v6.17.2.zip) 
1. [Bounce2](https://github.com/thomasfredericks/Bounce2/archive/v2.53.zip)
1. [ESPAsyncTCP >= c8ed544](https://codeload.github.com/me-no-dev/ESPAsyncTCP/zip/15476867dcbab906c0f1d47a7f63cdde223abeab)
1. [async-mqtt-client-0.8.1](https://github.com/marvinroger/async-mqtt-client/archive/v0.8.1.zip)
1. [ESPAsyncWebServer](https://codeload.github.com/me-no-dev/ESPAsyncWebServer/zip/f6fff3f91ebf45b91ca4cff2460d2febd61f1e27)

## Wiring Diagram

While the ZH03B is a 5V device the serial I/O lines are 3.3V compatible and will not exceed the maximum rated voltage of the ESP8266 GPIO lines.

| ZH03B 			| WRL-13678		| Signal  				|
| -------------   	|:-------------:| -----:				|
| 1. Vdd          	| 5V 			| Supply voltage		|
| 2. GND          	| Gnd			| Ground	  			|
| 3. NC           	| NC      	    | Not connected  		|
| 4. Rx Data      	| D3			| Serial port Rx@3.3V 	|
| 5. Tx Data      	| D2			| Serial port Tx@3.3V 	|
| 6. NC           	| NC			| Not connected  		|
| 7. NC           	| NC			| Not connected  		|
| 8. PWM Output   	| NC			| PWM output @ 3.3V		|

## The Data

The data is output on MQTT topics as follows:

1. <topic_prefix>/<device_id>/particle-size/summary
1. <topic_prefix>/<device_id>/particle-size/status
1. <topic_prefix>/<device_id>/particle-size/unit
1. <topic_prefix>/<device_id>/particle-size/value
1. <topic_prefix>/<device_id>/particle-size/PM1.0
1. <topic_prefix>/<device_id>/particle-size/PM2.5
1. <topic_prefix>/<device_id>/particle-size/PM10

Where "topic_prefix" and "device_id" are specified in the JSON configuration file.

The *status* topic provides the life-cycle and connection state of the node, one of "init", "ready" or "lost".

The *PM1.0*, *PM2.5* and *PM10* topics provide the calibrated particle density of 1.0, 2.5 and 10 μm diameter particles, as 
calculated by the sensor, in μg/m<sup>3</sup>.

The *value* topic contains the comma separated values of all 3 particle sizes:
```
17,21,23
```

The *value*, *PM1.0*, *PM2.5* and *PM10* topics are updated at the interval specified in the JSON configuration.

### Example Data

The figure below shows the PM10 data recorded in my house between the 3rd and 8th of December 2020. The peaks are typically
caused by smoke that is sometimes released from the [central heating fireplace](https://prity-bg.com/en/products/fireplaces/standard/prity-s3-w17/), 
when I open the fireplace door to add fresh wood.

![Typical PM10 values during winter, when the maintaining the fireplace releases smoke.](PM10_particle_history.png "PM10 in winter")

## OTA
[Over-the-Air (OTA) updates](https://homieiot.github.io/homie-esp8266/docs/3.0.1/others/ota-configuration-updates/) is 
enabled and supported using the included [scripts](https://github.com/homieiot/homie-esp8266/tree/v3.0/scripts/ota_updater).

In Arduino.cc, use CTRL-ALT-S to export the binary to the project directory.

To upload the binary, use a command similar to the following:
```bash
$ ota_updater.py -l <your.mqtt.server> -t "<topic_prefix>/" -i <device_id> <name_of_binary.bin>

```
where:

- *<your.mqtt.server>* is the name or IP address of your MQTT server,
- *<topic_prefix>* is your topic prefix,
- *<device_id>* is the ID of the device that you wish to upgrade, and
- *<name_of_binary.bin>* identifies the binary you wish to upload.