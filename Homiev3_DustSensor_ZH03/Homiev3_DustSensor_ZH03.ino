/**

  This program uses a ZH03 dust particle sensor.
    https://www.winsen-sensor.com/d/files/air-quality/zh03-series-laser-dust-module-v2_0.pdf
	https://www.winsen-sensor.com/d/files/ZH03B.pdf
  It runs continuously, reporting the counts at an interval.

  Original sensor code modified from:
    https://github.com/adafruit/Adafruit_Learning_System_Guides/blob/master/PMS5003_Air_Quality_Sensor/PMS5003_Arduino/PMS5003_Arduino.ino


  Using Homie convention v3.0.1
	https://github.com/homieiot/homie-esp8266/archive/v3.0.1.zip
    https://homieiot.github.io/specification/
    https://homieiot.github.io/homie-esp8266/docs/3.0.1/others/cpp-api-reference/


  Board:
    WRL-13678: Generic ESP8266, 1M Flash, 64kB FS (~470kB OTA)
	https://www.digikey.com/en/products/detail/sparkfun-electronics/WRL-13678/5725944 (obsolete)
	https://www.sparkfun.com/products/17146 (4MB flash)

  URLs:
	https://www.aliexpress.com/item/32964523505.html

   OTA Update:
    ota_updater.py -l nassy.lan -t "home/" -i ESP8266-dust-2 Homie_ZH03.ino.generic-v1.0.1.bin

  History:
    v2.0.0 : Renaming topics: <topic_prefix>/<device_id>/dust -> <topic_prefix>/<device_id>/particle-size
    v1.1.0 : Adding history of last 10 values of PM1.0, PM2.5, PM10
    v1.0.2 : Internal update, recompiling with latest version of the dependencies, adding to GitLab
    v1.0.1 : Value gets the histogram of values: PM1.0, PM2.5, PM10
    v1.0.0 : Original
*/

#include <Homie.h>

#include <SoftwareSerial.h>

// NB: You must change the Arduino Board setting to match the hardware
#define HW_DESC "Generic ESP 1M, WRL-13678"

#define FW_NAME "dust-wrl13678-zh03b"
#define FW_VERSION "2.0.0"

#define PAYLOAD_LEN 24

#define DEBUG_LEVEL 0

// Reporting interval
// 60000 = once per minute
unsigned long messageInterval = 60000;
unsigned long nextMessageTimeMs = 0;

HomieNode ZH03SensorNode("particle-size", "ZH03 sensor, air quality", "sensor");
HomieSetting<long> delaySetting("interval", "Reporting interval (s)");  // id, description

SoftwareSerial pmsSerial(2, 3); // RxPin, TxPin

// ------------------------------------------ ZH03 Sensor stuff -------------------

struct zh03data {
  uint16_t framelen;
  uint16_t unused1, unused2, unused3;
  uint16_t pm10_env, pm25_env, pm100_env;
  uint16_t unused4, unused5, unused6;
  uint16_t checksum;
};
struct zh03data data;
static_assert(sizeof(zh03data) == PAYLOAD_LEN - 2, "zh03data does not match payload size");

int8_t valid_data_received = 0; // set in the loop to indicate that valid data has been received from the sensor

// Last 10 measurements
#define HISTORY_COUNT 10
uint16_t PM01_history[HISTORY_COUNT];
uint16_t PM25_history[HISTORY_COUNT];
uint16_t PM10_history[HISTORY_COUNT];
// ---------------------------------------------------------------------------------

void onHomieEvent(const HomieEvent& event) {
  if  (event.type == HomieEventType::WIFI_CONNECTED) {
    Serial << " Event -> HomieEventType::WIFI_CONNECTED" << endl;
    Serial << "Wi-Fi connected, IP: " << event.ip << ", gateway: " << event.gateway << ", mask: " << event.mask << endl;

    const HomieInternals::ConfigStruct& config = Homie.getConfiguration();
    Serial << " Configuration name: " << config.name << endl;
    Serial << " Configuration deviceId: " << config.deviceId << endl;

  } else if (event.type == HomieEventType::MQTT_READY) {
    Serial << "MQTT connected" << endl;
  } else if (event.type == HomieEventType::MQTT_DISCONNECTED) {
    Serial << "MQTT disconnected, reason: " << (int8_t)event.mqttReason << endl;
    delay(500);
  }
}

void init_history(uint16_t *which, uint8_t count) {
  for (int i = 0; i < count; i++) {
    which[i] = 0;
  }
}

void update_history(uint16_t *which, uint8_t count, char* topic, uint16_t latest) {
  char buffer[100];
  int offset = 0;

  // Move the old values one to the left
  for (int i = 0; i < count-1; i++) {
    which[i] = which[i+1];
  }
  
  // Update the latest value
  which[count-1] = latest;
  
  // Create the string representation
  for (int i = 0; i < count; i++) {
    if (i < count - 1) {
      offset += snprintf(buffer+offset, sizeof(buffer)-offset, "%d,", which[i]);
    } else {
      offset += snprintf(buffer+offset, sizeof(buffer)-offset, "%d", which[i]);
    }
  }

  ZH03SensorNode.setProperty(topic).send(buffer);
}

void publish_history(uint16_t p1, uint16_t p25, uint16_t p10) {
  update_history(PM01_history, HISTORY_COUNT, "PM1.0_history", p1);
  update_history(PM25_history, HISTORY_COUNT, "PM2.5_history", p25);
  update_history(PM10_history, HISTORY_COUNT, "PM10_history", p10);
}

void setup() {
  // our debugging output
  Serial.begin(115200);

  Serial << endl << "\n\nZH03 Dust Particle Counter Sensor v" << FW_VERSION << endl;

  Homie.disableLedFeedback(); // Disable Homie LED, before Homie.setup()

  // Initialise history buffers
  init_history(PM01_history, HISTORY_COUNT);
  init_history(PM25_history, HISTORY_COUNT);
  init_history(PM10_history, HISTORY_COUNT);

  // sensor baud rate is 9600
  pmsSerial.begin(9600);

  Homie_setFirmware(FW_NAME, FW_VERSION);

  // Populate the node name with default + setting
  delaySetting.setDefaultValue(60);
  messageInterval = delaySetting.get() * 1000;

  Homie.onEvent(onHomieEvent);
  Homie.setSetupFunction(setupHandler).setLoopFunction(loopHandler);

  // Configure the ZH03 dust node
  ZH03SensorNode.advertise("summary");
  ZH03SensorNode.advertise("hardware");
  ZH03SensorNode.advertise("status");
  ZH03SensorNode.advertise("unit");
  ZH03SensorNode.advertise("value");
  ZH03SensorNode.advertise("PM1.0");
  ZH03SensorNode.advertise("PM2.5");
  ZH03SensorNode.advertise("PM10");
  ZH03SensorNode.advertise("PM1.0_history");
  ZH03SensorNode.advertise("PM2.5_history");
  ZH03SensorNode.advertise("PM10_history");

  Homie.setup();

  const HomieInternals::ConfigStruct& config = Homie.getConfiguration();
  Homie.getLogger() << "ZH03 Air Quality Sensor implementation v" << FW_VERSION << endl;
  Homie.getLogger() << "MQTT topic root: " << config.mqtt.baseTopic << config.deviceId << endl;
  Homie.getLogger() << "Reporting interval set to " << delaySetting.get() << " seconds" << endl;
}


void setupHandler() {
  char summary[100];
  snprintf(summary, sizeof(summary), "Reported every %ld seconds", delaySetting.get());

  // Configure the ZH03 node
  ZH03SensorNode.setProperty("unit").send("ug/m^3");
  ZH03SensorNode.setProperty("summary").send(summary);
  ZH03SensorNode.setProperty("hardware").send(HW_DESC);
  ZH03SensorNode.setProperty("status").send("Initialising");
}

/**
   Returns 0 on success, else -1 on no-data or -2 on error
*/
int8_t readPMSdata(Stream *s) {
  if (! s->available()) {
    return -1;
  }

  // Read a byte at a time until we get to the special '0x42' start-byte
  if (s->peek() != 0x42) {
    s->read();
    return -2;
  }

  // Now read all PAYLOAD_LEN bytes
  if (s->available() < PAYLOAD_LEN) {
    return -3;
  }

  uint8_t buffer[PAYLOAD_LEN + 2];
  uint16_t sum = 0;
  s->readBytes(buffer, PAYLOAD_LEN);

  // get checksum ready
  for (uint8_t i = 0; i < PAYLOAD_LEN - 2; i++) {
    sum += buffer[i];
  }

#if DEBUG_LEVEL>0
  for (uint8_t i = 0; i < PAYLOAD_LEN; i++) {
    Serial.print("0x"); Serial.print(buffer[i], HEX); Serial.print(", ");
  }
  Serial.println();
#endif

  // The data comes in endian'd, this solves it so it works on all platforms
  uint16_t ints = (PAYLOAD_LEN / 2) - 1;
  uint16_t buffer_u16[ints];
  for (uint8_t i = 0; i < ints; i++) {
    buffer_u16[i] = buffer[2 + i * 2 + 1];
    buffer_u16[i] += (buffer[2 + i * 2] << 8);
  }

  // put it into a nice struct :)
  memcpy((void *)&data, (void *)buffer_u16, PAYLOAD_LEN - 2);

  // Check the check-sum
  if (sum != data.checksum) {
    return -4;
  }

  // success!
  return 0;
}

void loopHandler() {
  unsigned long now = millis();

  if ((now + messageInterval) < nextMessageTimeMs) {
    nextMessageTimeMs = now + messageInterval;
    
    Homie.getLogger() << "millis() roll-over detected. Resetting timers." << " counts" << endl;
    ZH03SensorNode.setProperty("status").send("Roll-over detected");
    return;
  }

  int8_t result = readPMSdata(&pmsSerial);
  if (result == -4) {
    Homie.getLogger() << "Checksum error reading serial" << endl;
    ZH03SensorNode.setProperty("status").send("ERROR: Checksum");
  }

  if (result == 0) {
    valid_data_received = 1;
  }
  
  if (valid_data_received == 1) {
    // Time to read the sensor
    if (now >= nextMessageTimeMs) {
      nextMessageTimeMs = now + messageInterval;
      valid_data_received = 0;
      
      Homie.getLogger() << "PM 1.0: " << data.pm10_env << ", PM 2.5: " << data.pm25_env << ", PM 10: " << data.pm100_env  << endl;

      String pm10(data.pm10_env);
      String pm25(data.pm25_env);
      String pm100(data.pm100_env);

      ZH03SensorNode.setProperty("value").send(pm10 + ", " + pm25 + ", " + pm100);
      ZH03SensorNode.setProperty("PM1.0").send(pm10);
      ZH03SensorNode.setProperty("PM2.5").send(pm25);
      ZH03SensorNode.setProperty("PM10").send(pm100);

      publish_history(data.pm10_env, data.pm25_env, data.pm100_env);

      ZH03SensorNode.setProperty("status").send("OK");
    }
  } else {
    if (now >= nextMessageTimeMs) {
      nextMessageTimeMs = now + messageInterval / 10;
      ZH03SensorNode.setProperty("status").send("ERROR: " + String(result));
    }
  }
}

void loop() {
  Homie.loop();
}
